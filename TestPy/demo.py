import pickle
import cv2
from numba import jit
import numpy as np

# 是否是字的分类
with open('E:\\Development\\tmp\\TestData\\1\\cls.npy','rb') as f:
    C = np.load(f)[0]

# 每个点到中心线的距离
with open('E:\\Development\\tmp\\TestData\\1\\distance.npy','rb') as f:
    D = np.load(f)[0]

# 每个点的预测方向
with open('E:\\Development\\tmp\\TestData\\1\\offset.npy','rb') as f:
    O = np.load(f)[0]


# 第一个功能, cls进行二分类
def cls_bin(C,threshold=0.5,larger=True):
    if larger: return C>threshold
    else: return C<threshold

def guiyi(c,mi,mx):
    return max(min(c,mx),mi)

def save_picture(x,imgfile):
    if x.max()<=1: 
        x = x*255
    cv2.imwrite(imgfile,x)

@jit
def offset_diedai(C,D,O,loop=5):
    n,m = C.shape

    target = cls_bin(C,larger=True)
    D_b = cls_bin(D,larger=False)

    save_picture(target,'./target.jpg')
    save_picture(D_b,'./D_b.jpg')

    ret = np.zeros((n,m))
    for i in range(n):
        for j in range(m):
            if D_b[i,j] == False: 
                continue
            o_x,o_y=i,j
            x,y=o_x,o_y
            got_it = False
            for k in range(loop):
                dx,dy = O[x,y]
                nx,ny = int(x+dx+0.5),int(y+dy+0.5)
                nx = guiyi(nx,0,n-1)
                ny = guiyi(ny,0,m-1)
                if target[nx,ny] == True:
                    got_it = True
                    break
                x,y = nx,ny
            if got_it == True:
                ret[o_x,o_y] = 1
    return ret

R = offset_diedai(C,D,O,5)

save_picture(R,'./R.jpg')

# import IPython; IPython.embed()