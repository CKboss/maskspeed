import numpy as np

def save_npy_txt(a,path):
    a = a.copy()
    shape = a.shape
    a = a.reshape(-1)
    np.savetxt(path,a)
    lines = []
    with open(path,'r') as f:
        lines = f.readlines()
    with open(path,'w') as f:
        n = len(shape)
        f.write(str(n)+"\n")
        line = ""
        for i in range(n):
            line += str(shape[i])+" "
        line+='\n'
        f.write(line)
        for line in lines:
            f.write(line)

def load_npy_txt(txt_path,npy_path=None):
    with open(txt_path,'r') as f:
        lines = f.readlines()
    n = int(lines[0])
    shape = tuple(map(int,lines[1].strip().split(' ')))
    ret = np.zeros(shape).astype(np.float32)
    base = [1]
    for s in reversed(shape):
        base.append(base[-1]*s)
    base = list(reversed(base))
    def get_coord(pos):
        ret = []
        for cnt in range(1,len(base)):
            ret.append(pos//base[cnt])
            pos = pos % base[cnt]
            cnt += 1
        return ret
    for pos in range(0,base[0]):
        coord = get_coord(pos)
        v = float(lines[pos+2].strip())
        ret[tuple(coord)] = v
        if coord[0]==0 and coord[1]<10:
            print(coord,v)
    if npy_path is not None: 
        np.save(npy_path,ret)
    return ret