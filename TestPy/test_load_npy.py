from tools import save_npy_txt, load_npy_txt
import numpy as np


with open('E:\\Development\\tmp\\TestData\\1\\cls.npy','rb') as f:
    C = np.load(f)[0]
save_npy_txt(C,'./C.txt')


# 每个点到中心线的距离
with open('E:\\Development\\tmp\\TestData\\1\\distance.npy','rb') as f:
    D = np.load(f)[0]
save_npy_txt(D,'./D.txt')

# 每个点的预测方向
with open('E:\\Development\\tmp\\TestData\\1\\offset.npy','rb') as f:
    O = np.load(f)[0]
save_npy_txt(O,'./O.txt')

import IPython; IPython.embed()