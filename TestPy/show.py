import numpy as np
import cv2
from tools import load_npy_txt

O = load_npy_txt('./out_ret.txt')
O = O*255
cv2.imwrite('./out_ret.jpg',O)


O1 = load_npy_txt('./out_C.txt')
O1 = O1*255
cv2.imwrite('./out_C.jpg',O1)

O2 = load_npy_txt('./out_D.txt')
O2 = O2*255
cv2.imwrite('./out_D.jpg',O2)

import IPython; IPython.embed()
