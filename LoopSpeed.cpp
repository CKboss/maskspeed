#include "LoopSpeed.h"

#include <cstdio>
#include <fstream>
#include <vector>
#include <iostream>
#include <time.h>

#include <cuda.h>
#include <cuda_runtime_api.h>

using namespace std;

// extern cuda function
extern void filter_with_distance_gpu(float* matrix, int* ret, int H, int W, float threshold, bool larger);
extern void move_point_gpu(int* target, int* D_mask, float* offset, int* ret, int H, int W, int loop_limit);

// 从3个txt文件里读取npy信息
LoopSpeed::LoopSpeed(string cls_file, string distan_file, string offset_file)
{
	this->cls_npy_file = cls_file;
	this->distan_npy_file = distan_file;
	this->offset_npy_file = offset_file;

	this->cls = this->load_npy_from_txt(this->cls_npy_file);
	this->distant = this->load_npy_from_txt(this->distan_npy_file);
	this->offset = this->load_npy_from_txt(this->offset_npy_file);
}

Npy_Data LoopSpeed::load_npy_from_txt(string npyfile)
{
	ifstream fin(npyfile.c_str());
	int nshape = 0;
	int n_data_num = 1;
	fin >> nshape;
	Npy_Data npy;
	for (int i = 0; i < nshape; i++)
	{
		int x;
		fin >> x;
		npy.shape.push_back(x);
		n_data_num *= x;
	}
	for (int i = 0; i < n_data_num; i++)
	{
		float x;
		fin >> x;
		npy.data.push_back(x);
	}
	return npy;
}

void LoopSpeed::save_npy_to_txt(Npy_Data* npy, string out_txt_file)
{
	ofstream fout(out_txt_file.c_str());
	fout << npy->shape.size() << endl;
	int n = 1;
	for (int i = 0, sz = npy->shape.size(); i < sz; i++)
	{
		fout << npy->shape[i] << " ";
		n = n * npy->shape[i];
	}
	fout << endl;
	for (int i = 0; i < n; i++)
	{
		fout << npy->data[i] << endl;
	}
	cout << endl;
}

void LoopSpeed::loop_with_offset()
{
	vector<int> shape = this->cls.shape;
	int H = shape[0];
	int W = shape[0];

	float cls_threshold = 0.5;
	float dis_threshold = 0.5;

	// HxW
	float* dev_C;
	// HxW
	float* dev_D;
	// HxWx2
	float* dev_O;
	// HxW
	int* C_mask;
	int* D_mask;
	int* dev_R;
	int* Ret = (int *)malloc(sizeof(int) * H * W);

	clock_t begin, end;

	begin = clock();

	cudaMalloc((void**)& dev_C, sizeof(float) * H * W);
	cudaMalloc((void**)& dev_D, sizeof(float) * H * W);
	cudaMalloc((void**)& dev_O, sizeof(float) * H * W * 2);
	cudaMalloc((void**)& C_mask, sizeof(int) * H * W);
	cudaMalloc((void**)& D_mask, sizeof(int) * H * W);
	cudaMalloc((void**)& dev_R, sizeof(int) * H * W);

	cudaMemcpy(dev_C, vector_to_arr(this->cls.data), sizeof(float)*this->cls.data.size(), cudaMemcpyHostToDevice);
	cudaMemcpy(dev_D, vector_to_arr(this->distant.data), sizeof(float)*this->distant.data.size(), cudaMemcpyHostToDevice);
	cudaMemcpy(dev_O, vector_to_arr(this->offset.data), sizeof(float)*this->offset.data.size(), cudaMemcpyHostToDevice);
	cudaMemset(dev_R, 0, sizeof(int) * H * W);

	filter_with_distance_gpu(dev_C, C_mask, H, W, 0.5,true);
	filter_with_distance_gpu(dev_D, D_mask, H, W, 0.5,false);

	//
	Npy_Data out_C;
	int* Host_C = (int *)malloc(sizeof(int) * H * W);
	cudaMemcpy(Host_C, C_mask, sizeof(int) * H * W, cudaMemcpyDeviceToHost);
	out_C.shape = vector<int>{ H,W };
	out_C.data = vector<float>{};
	for (int i = 0; i < H * W; i++)
	{
		out_C.data.push_back(Host_C[i]);
	}
	this->save_npy_to_txt(&out_C,"E:\\Development\\tmp\\TestPy\\out_C.txt");
	//
	Npy_Data out_D;
	int* Host_D = (int *)malloc(sizeof(int) * H * W);
	cudaMemcpy(Host_D, D_mask, sizeof(int) * H * W, cudaMemcpyDeviceToHost);
	out_D.shape = vector<int>{ H,W };
	out_D.data = vector<float>{};
	for (int i = 0; i < H * W; i++)
	{
		out_D.data.push_back(Host_D[i]);
	}
	this->save_npy_to_txt(&out_D,"E:\\Development\\tmp\\TestPy\\out_D.txt");
	//

	move_point_gpu(C_mask, D_mask, dev_O, dev_R, H, W, 5);

	cudaMemcpy(Ret, dev_R, sizeof(int) * H * W, cudaMemcpyDeviceToHost);

	Npy_Data out_npy;
	out_npy.shape = vector<int>{ H,W };
	out_npy.data = vector<float>{};
	for (int i = 0; i < H * W; i++)
	{
		out_npy.data.push_back(Ret[i]);
	}
	this->save_npy_to_txt(&out_npy,"E:\\Development\\tmp\\TestPy\\out_ret.txt");

	free(Ret);
	cudaFree(dev_C);
	cudaFree(dev_D);
	cudaFree(dev_O);
	cudaFree(dev_R);
	cudaFree(C_mask);
	cudaFree(D_mask);

	end = clock();
	cout << "all right total time: " << (end - begin) << endl;
	cout << "Ret size: " << sizeof(Ret) << endl;
}

// test
///////////////////////////////////////////////

void LoopSpeed::test_1()
{
	vector<int> shape = this->cls.shape;
	this->loop_with_offset();
	printf("every thing is right.\n");
}