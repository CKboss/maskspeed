#pragma once

#include <iostream>
#include <cstdio>
#include <vector>
#include "PosChange.h"

void test()
{
	PosChange pc(vector<int>{3, 4, 5});
	for (int i = 0; i < 3; i++)
	{
		cout << pc.base[i] << ",";
	}
	cout << endl;
	cout << pc.coord2pos(vector<int>{1, 2, 3}) << endl;

	vector<int> ret = pc.pos2coord(33);
	cout << pc.shape.size() << endl;
	cout << ret.size() << endl;

	for (int i = 0; i < 3; i++)
	{
		cout << ret[i] << ",";
	}
	cout << endl;
}