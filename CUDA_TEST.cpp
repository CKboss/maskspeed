#include <iostream>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include "LoopSpeed.h"
#include "TestPosChange.h"

using namespace std;

int main(int argc, const char* argv[])
{

	LoopSpeed ls("E:\\Development\\tmp\\TestPy\\C.txt",
				 "E:\\Development\\tmp\\TestPy\\D.txt",
		         "E:\\Development\\tmp\\TestPy\\O.txt");
	ls.test_1();
	printf("Success!\n");
	return 0;
}