#include <math.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <stdio.h>
#include <cassert>
#include <algorithm>
#include <thrust/device_vector.h>

using namespace std;

#include "PosChange.h"

__global__ void filter_by_distance(float* matrix, int* ret, int H, int W, float threhold, int K1, int K2,bool larger=true)
{
	int grid_dim = gridDim.x;
	int block_dim = blockDim.x;

	int block_x = blockIdx.x;
	int thread_x = threadIdx.x;

	for(int i=0;i<=K1;i++)
	{
		for (int j = 0; j <= K2; j++)
		{
			int X = block_x + i * grid_dim;
			int Y = thread_x + j * block_dim;
			if (X >= H || Y >= W) continue;
			int pos = X*W + Y;
			if (larger==true)
			{
				if (matrix[pos] >= threhold) ret[pos] = 1;
				else ret[pos] = 0;
			}
			else
			{
				if (matrix[pos] <= threhold) ret[pos] = 1;
				else ret[pos] = 0;
			}
		}
	}
}

__global__ void move_point(int* Target, int* D_mask, float* Offset, int* ret, int loop_limit, int H, int W, int K1, int K2)
{

	int grid_dim = gridDim.x;
	int block_dim = blockDim.x;

	int block_x = blockIdx.x;
	int thread_x = threadIdx.x;

	for (int i = 0; i <= K1; i++)
	{
		for (int j = 0; j <= K2; j++)
		{
			int OX = block_x + i * grid_dim;
			int OY = thread_x + j * block_dim;
			if (OX >= H || OY >= W) continue;
			int origin_pos = OX*W+OY;

			// 不在范围内, 跳过该点
			if (D_mask[origin_pos] == 0) continue;

			int x = OX, y = OY, pos = origin_pos;
			int in_it = 0;
			for (int k = 0; k < loop_limit && in_it==0; k++)
			{
				float dx = Offset[pos*2];
				float dy = Offset[pos*2+1];
				int nx = max(min(int(x + dx + 0.5), H-1), 0);
				int ny = max(min(int(y + dy + 0.5), W-1), 0);
				int npos = nx * W + ny;
				if (Target[npos] == 1) in_it = 1;
				x = nx;
				y = ny;
				pos = npos;
			}
			ret[origin_pos] = in_it;
		}
	}
}

int GRID_DIM = 255;
int BLOCK_DIM = 255;

extern void filter_with_distance_gpu(float* matrix, int* ret, int H, int W, float threshold,bool larger)
{

	int K1 = H / GRID_DIM + 1;
	int K2 = W / BLOCK_DIM + 1;

	// grid 大小 128, block 大小 128
	// 遍历完 矩阵需要 挪动 K1 x K2 次

	filter_by_distance <<< GRID_DIM, BLOCK_DIM >>> (matrix, ret, H, W, threshold, K1, K2,larger);
}

extern void move_point_gpu(int* target, int* D_mask, float* offset, int* ret, int H, int W, int loop_limit)
{

	int K1 = H / GRID_DIM + 1;
	int K2 = W / BLOCK_DIM + 1;

	move_point <<< GRID_DIM,BLOCK_DIM >>> (target, D_mask, offset, ret, loop_limit, H, W, K1, K2);
}
