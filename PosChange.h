#pragma once

#include <vector>

using namespace std;

class PosChange
{
public:
	PosChange(vector<int> shape);
	int coord2pos(vector<int> coord);
	vector<int> pos2coord(int pos);

	vector<int> shape;
	vector<int> base;
};

