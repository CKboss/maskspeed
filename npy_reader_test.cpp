#include <iostream>
#include <vector>
#include <fstream>

#include "Test/npy_reader_test.h"

using namespace std;

void test_load()
{
	ifstream fin("E:\\Development\\tmp\\a.txt");
	int nshape = 0;
	int n_data_num = 1;
	fin >> nshape;
	vector<int> shape;
	vector<double> data;
	for (int i = 0; i < nshape; i++)
	{
		int x;
		fin >> x;
		shape.push_back(x);
		n_data_num *= x;
	}
	for (int i = 0; i < n_data_num; i++)
	{
		double x;
		fin >> x;
		data.push_back(x);
	}

	cout << "nshape: " << nshape << endl;
	for (int i = 0; i < nshape; i++)
	{
		cout << shape[i] << ",";
	}
	cout << endl;

	for (int i = 0; i < shape[0]; i++)
	{
		for (int j = 0; j < shape[1]; j++)
		{
			int pos = i * shape[1] + j;
			cout << i << "," << j << " : " << data[pos] << endl;
		}
	}
}

void test_load_3d()
{
	ifstream fin("E:\\Development\\tmp\\a3d.txt");

	int nshape = 0;
	int n_data_num = 1;

	fin >> nshape;

	vector<int> shape;
	vector<double> data;

	for (int i = 0; i < nshape; i++)
	{
		int x;
		fin >> x;
		shape.push_back(x);
		n_data_num *= x;
	}
	for (int i = 0; i < n_data_num; i++)
	{
		double x;
		fin >> x;
		data.push_back(x);
	}

	cout << "nshape: " << nshape << endl;
	for (int i = 0; i < nshape; i++)
	{
		cout << shape[i] << ",";
	}
	cout << endl;

	for (int k = 0; k < shape[0]; k++)
	{
		for (int i = 0; i < shape[1]; i++)
		{
			for (int j = 0; j < shape[2]; j++)
			{
				int pos = k * shape[1] * shape[2] + i * shape[2] + j;
				cout << k << "," << i << "," << j << " : " << data[pos] << endl;
			}
		}
	}
}

void NPY::test1()
{
	test_load_3d();
	printf("hello world\n");
}