#include "PosChange.h"
#include <vector>

using namespace std;

PosChange::PosChange(vector<int> shape)
{
	this->shape = vector<int>(shape);
	int b = 1;
	for (int i = this->shape.size() - 1; i >= 0; i--)
	{
		this->base.push_back(b);
		b = b * this->shape[i];
	}
	reverse(this->base.begin(), this->base.end());
}

int PosChange::coord2pos(vector<int> coord)
{
	int pos = 0;
	for (int i = coord.size() - 1; i >= 0; i--)
	{
		pos += coord[i] * this->base[i];
	}
	return pos;
}

vector<int> PosChange::pos2coord(int pos)
{
	vector<int> ret;
	for (int i = 0, sz = this->shape.size(); i < sz; i++)
	{
		ret.push_back(pos / this->base[i]);
		pos = pos % this->base[i];
	}
	return ret;
}
