#pragma once

#include <string>
#include <vector>

using namespace std;

struct Npy_Data
{
	vector<int> shape;
	vector<float> data;
};

class LoopSpeed
{
public:

	LoopSpeed(string cls_file, string distant_file, string offset_file);

	// 一些和npy交互的函数
	Npy_Data load_npy_from_txt(string npy_file);
	void save_npy_to_txt(Npy_Data* npy, string out_txt_file);
	// 未实现
	void Load_npy();

	// 最主要的需要进行优化的函数
	void loop_with_offset();

	// 测试函数
	void test_1();

	/////////////////////////////////////////////////////////////
	// 一些变量
	string cls_npy_file;
	string distan_npy_file;
	string offset_npy_file;

	Npy_Data cls, distant, offset;
};

// 一些工具函数
template <typename T>
T* vector_to_arr(vector<T> v)
{
	int sz = v.size();
	T* array = new T[sz];
	for (int i = 0; i < sz; i++)
	{
		array[i] = v[i];
	}
	return array;
}